import argparse
import os
import subprocess
from pathlib import Path

from deepspeech_pytorch.data.data_opts import add_data_opts
from deepspeech_pytorch.data.utils import create_manifest

parser = argparse.ArgumentParser(description='Processes and downloads LibriSpeech dataset.')
parser = add_data_opts(parser)
parser.add_argument("--source-dir", help="Directory source the dataset.")
parser.add_argument("--target-dir", default='custom_dataset/', type=str, help="Directory to store the dataset.")
parser.add_argument('--split-to-use', default=0.8, type=float,
                    help='split train/test')
args = parser.parse_args()


def _preprocess_transcript(phrase):
    return phrase.strip().lower()


def _process_file(wav_dir, txt_dir, path_wav_file, path_txt_file, base_filename):
    assert os.path.exists(wav_dir) and os.path.exists(txt_dir)
    extension_voice = base_filename.split(".")[-1]
    wav_recording_path = os.path.join(wav_dir, base_filename.replace(extension_voice, "wav"))
    subprocess.call(["sox {}  -r {} -b 16 -c 1 -e signed-integer {}".format(path_wav_file, str(args.sample_rate), wav_recording_path)],
                    shell=True)
    # process transcript
    txt_transcript_path = os.path.join(txt_dir, base_filename.replace(extension_voice, "txt"))

    assert os.path.exists(path_txt_file), "Transcript file {} does not exist.".format(path_txt_file)
    transcription = open(path_txt_file).read().strip("\n")

    with open(txt_transcript_path, "w") as f:
        f.write(_preprocess_transcript(transcription))
        f.flush()


def main():
    target_dl_dir = args.target_dir
    source_dir = args.source_dir
    if not os.path.exists(target_dl_dir):
        os.makedirs(target_dl_dir)
    else:
        os.remove(target_dl_dir)
        os.makedirs(target_dl_dir)

    if not os.path.exists(os.path.join(source_dir, "tmp_keys")):
        os.makedirs(os.path.join(source_dir, "tmp_keys"))
    else:
        os.remove(os.path.join(source_dir, "tmp_keys"))
        os.makedirs(os.path.join(source_dir, "tmp_keys"))

    custom_datasets = {}
    with open(f"{source_dir}/metadata.csv", "r") as f:
        re_metadata = []
        for line in f.readlines():
            path_wav, tran_wav = line.split("|")
            trans_file_txt = f"{source_dir}/tmp_keys/{Path(path_wav).stem}.txt"
            re_path_wav = f"{source_dir}/wavs/{path_wav}"
            wtxt = open(trans_file_txt, 'w')
            wtxt.write(tran_wav)
            wtxt.close()
            re_metadata.append(f"{re_path_wav},{trans_file_txt}")
    custom_datasets.update({
        "train": re_metadata[:int(len(re_metadata) * 0.8)],
        "eval": re_metadata[int(len(re_metadata) * 0.8):]
    })

    for split_type, lst_libri_urls in custom_datasets.items():
        split_dir = os.path.join(target_dl_dir, split_type)
        if not os.path.exists(split_dir):
            os.makedirs(split_dir)
        split_wav_dir = os.path.join(split_dir, "wav")
        if not os.path.exists(split_wav_dir):
            os.makedirs(split_wav_dir)
        split_txt_dir = os.path.join(split_dir, "txt")
        if not os.path.exists(split_txt_dir):
            os.makedirs(split_txt_dir)
        for url in lst_libri_urls:
            wav_file, txt_file = url.split(",")
            base_filename = os.path.basename(wav_file)
            _process_file(wav_dir=split_wav_dir, txt_dir=split_txt_dir, path_wav_file=wav_file, path_txt_file=txt_file,
                          base_filename=base_filename)

        print("Finished {}".format("Custom Dataset"))

        if split_type == 'train':  # Prune to min/max duration
            create_manifest(
                data_path=split_dir,
                output_name='custom_' + split_type + '_manifest.json',
                manifest_path=args.manifest_dir,
                min_duration=args.min_duration,
                max_duration=args.max_duration,
                num_workers=args.num_workers
            )
        else:
            create_manifest(
                data_path=split_dir,
                output_name='custom_' + split_type + '_manifest.json',
                manifest_path=args.manifest_dir,
                num_workers=args.num_workers
            )


if __name__ == "__main__":
    main()
